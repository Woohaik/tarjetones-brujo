# GUIA DE USO

* Instalar las dependencias
```bash 
npm install
```
* Editar el `.env` para agregar el matomo y kw correspondiente

* Remplazar la plantilla en el directorio /platilla (Solo debe haber un fichero en este directorio) 

* Asegurarse de que exista el directorio `/dist` en la raiz si no existe crearlo.

* Ejecutar comando

```bash
npm run dev
```

* Los tarjetonesse generaran en el directorio `/dist`
