export const SEDES_TO_CONVERT = [
    "AR",
    "AO",
    "BO",
    "BR",
    "CL",
    "CO",
    "CR",
    "CV",
    "DO",
    "EC",
    "ES",
    "GA",
    "GT",
    "HN",
    "IT",
    "MX",
    "MZ",
    "NI",
    "PA",
    "PE",
    "PR",
    "PT",
    "PY",
    "SV",
    "US",
    "UY",
    "VE"
];


type IFUNIBER_LOGOS = {
    [key: string]: string
}

type MASTER_NAMES_OPTION = { plural: string, singular: string }

type IMASTER_NAMES_BY_SEDES = {
    [key: string]: MASTER_NAMES_OPTION
}

type MASTER_NAMES_ENUM = "maestria" | "master" | "masterNoEs" | "magister" | "mestrado"


type IMASTER_NAMES = {
    [key in MASTER_NAMES_ENUM]: MASTER_NAMES_OPTION
}

export const MASTER_NAMES: IMASTER_NAMES = {
    maestria: {
        plural: "Maestrías",
        singular: "Maestría"
    },
    master: {
        plural: "Másters",
        singular: "Máster"
    },
    masterNoEs: {
        plural: "Masters",
        singular: "Master"
    },
    magister: {
        plural: "Magísteres",
        singular: "Magíster"
    },
    mestrado: {
        plural: "Mestrados",
        singular: "Mestrado"
    }
};

export const MASTER_NAMES_BY_SEDE: IMASTER_NAMES_BY_SEDES = {
    default: MASTER_NAMES.maestria,
    // NON-DEFAULT
    AR: MASTER_NAMES.master,
    ES: MASTER_NAMES.master,
    UY: MASTER_NAMES.master,
    CL: MASTER_NAMES.magister,
    // Portugueses
    BR: MASTER_NAMES.mestrado,
    CV: MASTER_NAMES.masterNoEs,
    PT: MASTER_NAMES.masterNoEs,
    MZ: MASTER_NAMES.masterNoEs,
    AO: MASTER_NAMES.mestrado,
    // FINISH NON-DEFAULT
};

export const FUNIBER_LOGOS: IFUNIBER_LOGOS = {
    default: "https://emkt.funiber.org/images/logos/logo-funiber-es-192.png",
    // NON-DEFAULT
    EC: "https://emkt.funiber.org/images/logos/logo-funiber-ec-192.png",
    US: "https://emkt.funiber.org/images/logos/logo-funiber-en-192.png",
    CO: "https://emkt.funiber.org/images/logos/logo-funiber-co-uy-192.png",
    UY: "https://emkt.funiber.org/images/logos/logo-funiber-co-uy-192.png",
    PT: "https://emkt.funiber.org/images/logos/logo-funiber-pt-192.png",
    CV: "https://emkt.funiber.org/images/logos/logo-funiber-pt-192.png",
    AO: "https://emkt.funiber.org/images/logos/logo-funiber-pt-192.png",
    BR: "https://emkt.funiber.org/images/logos/logo-funiber-pt-192.png",
    MZ: "https://emkt.funiber.org/images/logos/logo-funiber-pt-192.png",
    IT: "https://emkt.funiber.org/images/logos/logo-funiber-it-192.png",
    GA: "https://emkt.funiber.org/images/logos/logo-funiber-fr-192.png"
    // FINISH NON-DEFAULT
};

export const RVOE_RexExp: RegExp = /(?=<span class="color-primary fz-13">)(.*?)(?=span>)/ms

export const TO_REPLACE_FURRIEL_STRING = "FURRIEL TO REPLACE";

export const SEDES_PT = ["MZ", "PT", "AO", "BR", "CV"];

export const SEDES_FR = ["GA"];
