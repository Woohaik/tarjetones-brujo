require("dotenv").config();
import xlsx from 'node-xlsx';
import { ISede, ISedesOpt } from '../types';
import { SEDES_FR, SEDES_PT, TO_REPLACE_FURRIEL_STRING } from './constants';

const MATOMO = process.env.MATOMO as string;
const KW = process.env.KW as string;
const queryParams = KW + MATOMO;


const emptySede: ISede = { img: "", furriel: "", footer: "" }

const readFooters = (objectToMute: ISedesOpt): void => {
    const workSheetsFromFile = xlsx.parse(`${__dirname}/../../excels/footers.xlsx`);
    // Extractting FUNIVER footers
    const funiberFooterTable = workSheetsFromFile.find(table => table.name === "FOOTERS FUNIBER 2021")
    // Add footer info
    if (!funiberFooterTable) throw new Error("No esta la ventana de footers");
    funiberFooterTable.data.forEach((footerData) => {
        const parsedData: string[] = footerData as string[]
        if (parsedData[1]) {
            if (!objectToMute[parsedData[0]]) {
                objectToMute[parsedData[0]] = { ...emptySede }
            }
            const match = (parsedData[1].match(/(?<=href="https:\/\/)(.*?)(?=" target?)/) ?? [])[0]
            if (!match) if (parsedData[1] !== "CÓDIGO FOOTER") throw new Error("Footer href no encontrado bien xd")
            objectToMute[parsedData[0]].footer = parsedData[1].replace("<div ", '<div id="footer" ').replace(match, (match + TO_REPLACE_FURRIEL_STRING))
        }
    });
};

const readFurrielNPixel = (objectToMute: ISedesOpt): void => {
    const workSheetsFromFile = xlsx.parse(`${__dirname}/../../excels/Furriels Áreas y Programas.xlsx`);
    const funiberFurrielTable = workSheetsFromFile.find(table => table.name === "Furriels FUNIBER"); // Extraer FUNIVER furriels
    // Add furriels info
    if (!funiberFurrielTable) throw new Error("No esta la ventana de furriels");
    funiberFurrielTable.data.forEach((furrielData) => {
        const parsedData: string[] = furrielData as string[]
        if (objectToMute[parsedData[1]]) {
            objectToMute[parsedData[1]].furriel = parsedData[2] + queryParams
            objectToMute[parsedData[1]].img = parsedData[6]
        }
    })
};

export const readExcels = (): ISedesOpt => {
    const sedesInfo: ISedesOpt = {}
    readFooters(sedesInfo)
    readFurrielNPixel(sedesInfo)
    console.log("MATOMO USADO: ", MATOMO)
    console.log("KW USADO: ", KW)
    return sedesInfo;
};

export const toLowerCaseAndAccent = (text: string): string => {
    text = text.toLowerCase();
    text = text.normalize('NFD').replace(/[\u0300-\u036f]/g, "")
    return text;
}

export const isSedePt = (sede: string): boolean => SEDES_PT.some(ptSedes => ptSedes === sede);

export const isSedeFr = (sede: string): boolean => SEDES_FR.some(frSedes => frSedes === sede);

export const funiber_domain_name_replacer = (sede: string): string => {
    if (sede === "MZ" || sede === "AO" || sede === "BR") return "funiber.org.br/m";
    if (sede === "PT" || sede === "CV") return "funiber.pt/m";
    if (sede === "GA") return "funiber.fr/m";
    return "funiber.org/m";
}
