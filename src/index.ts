import { funiber_domain_name_replacer, isSedeFr, isSedePt, readExcels, toLowerCaseAndAccent } from "./Utils"
import fs from "fs";
import { SEDES_TO_CONVERT, TO_REPLACE_FURRIEL_STRING, FUNIBER_LOGOS, MASTER_NAMES_BY_SEDE } from './Utils/constants';
import { ISedesOpt } from "./types";
const DomParser = require("dom-parser");
const parser = new DomParser();

const excelData: ISedesOpt = readExcels();
const plantillaDir = fs.readdirSync(`${__dirname}/../plantilla`); // Directorio de plantilla
if (plantillaDir.length === 0) throw new Error("No hay plantilla");

const datosTemplate = { nombre: plantillaDir[0], campaña: plantillaDir[0].split("_")[0].replace(/[0-9]/g, ''), sede: plantillaDir[0].split("_")[1].split(".")[0].toUpperCase() }
const logoFuniberDeTemplate = FUNIBER_LOGOS[datosTemplate.sede] ?? FUNIBER_LOGOS.default;
const templateMasterName = MASTER_NAMES_BY_SEDE[datosTemplate.sede] ?? MASTER_NAMES_BY_SEDE.default;


// READ PLANTILLA xd
let htmlTemplate = fs.readFileSync(`${__dirname}/../plantilla/${datosTemplate.nombre}`).toString('utf8').replace(/---/g, "").replace(`subject: ${datosTemplate.sede} | ${datosTemplate.campaña}`, "")

const htmlSedes = SEDES_TO_CONVERT.map(sede => {
    let htmlToMovexd = htmlTemplate;
    htmlToMovexd = htmlToMovexd.replace(/(?=<img)(.*?)(?=\/>)/ms, excelData[sede].img.replace("/>", 'id="pixel" ')) // Pixel de seguimiento
        .replace(`_${datosTemplate.sede.toLocaleLowerCase()}`, `_${sede.toLocaleLowerCase()}`)
        .replace(/(?<=\?furriel).*?(?=")/g, excelData[sede].furriel.replace("?furriel", "")); // ACA REMPLAZA EL FURRIEN PORQUE EL REGEX YA LO INCLUYE
    const INICIOFOOTER = parser.parseFromString(htmlToMovexd).getElementById('footer').outerHTML.split("\n")[0];
    htmlToMovexd = htmlToMovexd.replace(INICIOFOOTER, "INICIO FOOTER")
    const auxxxx = htmlToMovexd.split("INICIO FOOTER")[1].split("</div>")[0];

    htmlToMovexd = htmlToMovexd.replace(auxxxx, excelData[sede].footer
        .replace("</div>", "")).replace("INICIO FOOTER", "")
        .replace(TO_REPLACE_FURRIEL_STRING, excelData[sede].furriel);

    //  Remplazar logo funiber
    htmlToMovexd = htmlToMovexd.replace(logoFuniberDeTemplate, FUNIBER_LOGOS[sede] ?? FUNIBER_LOGOS.default);

    // Si tiene agregada extension de pais Ejemplo(funiber.org.mx) => funiber.org
    htmlToMovexd = htmlToMovexd.replace(`funiber.org.${datosTemplate.sede.toLowerCase()}`, "funiber.org");

    const masterNameSede = MASTER_NAMES_BY_SEDE[sede] ?? MASTER_NAMES_BY_SEDE.default;

    // Cambiar master por maestria etc... Donde sea necesario.
    // Primero cambiar los de las url (que no llevan tildes y es todo en minuscula)
    // Remover los plurales primero
    htmlToMovexd = htmlToMovexd
        .replace(new RegExp(toLowerCaseAndAccent(templateMasterName.plural), "g"), toLowerCaseAndAccent(masterNameSede.plural))
        .replace(new RegExp(toLowerCaseAndAccent(templateMasterName.singular), "g"), toLowerCaseAndAccent(masterNameSede.singular));

    htmlToMovexd = htmlToMovexd.replace(new RegExp(templateMasterName.plural, "g"), masterNameSede.plural);

    htmlToMovexd = htmlToMovexd.replace(new RegExp(templateMasterName.singular, "g"), masterNameSede.singular);


    // Para las sedes en pt el url funiber.org etc es diferente. Así que cambiarlo. 
    if (isSedePt(sede) || isSedeFr(sede)) {
        //
        htmlToMovexd = htmlToMovexd.replace(/funiber\.org\/m/g, funiber_domain_name_replacer(sede))
    }

    // Obtener nombre de maestria de la plantilla

    const BASE = `---\nsubject: ${sede} | ${datosTemplate.campaña}\n---`;
    return {
        sede: sede,
        html: BASE + htmlToMovexd
    };
})

// Borrar contenido de la carpeta dist antes de meter mas cambios
// PRimero leer todos los ficheros en el archivo 
const distContent = fs.readdirSync(`${__dirname}/../dist`);
distContent.forEach((oldDistFile) => {
    try {
        fs.unlinkSync(`${__dirname}/../dist/${oldDistFile}`)
    } catch (err) {
        console.error('Something wrong happened removing the file', err)
    }
});

// Ahora borar uno con uno

htmlSedes.forEach(htmlSede => {
    fs.writeFileSync(`${__dirname}/../dist/${datosTemplate.nombre.replace(`_${datosTemplate.sede.toLocaleLowerCase()}`, `_${htmlSede.sede.toLocaleLowerCase()}`)}`, htmlSede.html);
});
