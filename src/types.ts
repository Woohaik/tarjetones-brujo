export interface ISede {
    footer: string, furriel: string, img: string
}
export interface ISedesOpt {
    [key: string]: ISede
}